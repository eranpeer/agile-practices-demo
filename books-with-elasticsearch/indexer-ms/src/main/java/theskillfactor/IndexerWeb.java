package theskillfactor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import theskillfactor.demo.BooksMain;
import theskillfactor.demo.JsonSource;

@Configuration
@EnableAutoConfiguration
@ComponentScan
// @EnableTransactionManagement
// @Import(DemoMain.class)
@Import(BooksMain.class)
public class IndexerWeb {

	private static final Logger log = LoggerFactory.getLogger(IndexerWeb.class);

	@Bean
	JsonSource jsonSource() {
		return new JsonSourceImpl();
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(IndexerWeb.class, args);
	}

}