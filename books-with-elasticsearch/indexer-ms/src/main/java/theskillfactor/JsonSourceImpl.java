package theskillfactor;

import java.util.function.Consumer;

import theskillfactor.demo.JsonSource;

public class JsonSourceImpl implements JsonSource {

	private Consumer<String> handler;

	@Override
	public void setDataHandler(Consumer<String> handler) {
		this.handler = handler;
	}
	
	void simulateDataOnSocket(String json){
		handler.accept(json);
	}
}