package theskillfactor.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import theskillfactor.demo.BooksStorage.Book;


@Component
public class BooksIndexer {

	private static final class JsonBook implements Book {
		JsonElement jsonElement;
		
		public JsonBook(JsonElement jsonElement) {
			this.jsonElement = jsonElement;
		}

		@Override
		public String title() {
			return jsonElement.getAsJsonObject().get("title").getAsString();
		}

		@Override
		public String author() {
			return jsonElement.getAsJsonObject().get("author").getAsString();
		}
	}

	JsonSource dataSource;
	BooksStorage storage;

	public BooksIndexer(JsonSource dataSource, BooksStorage storage) {
		this.dataSource = dataSource;
		this.storage = storage;
		dataSource.setDataHandler(this::handle);
	}

	private void handle(String json) {
		JsonParser parser = new JsonParser();
		JsonArray jArray = parser.parse(json).getAsJsonArray();
		List<Book> books = toBooks(jArray);
		store(books);
	}

	private void store(List<Book> books) {
		for (Book book : books) {
			storage.store(book);
		}
	}

	private List<Book> toBooks(JsonArray booksJson) {
		List<Book> books = new ArrayList<>();
		for (JsonElement bookElement : booksJson) {
			Book book = new JsonBook(bookElement);
			books.add(book);
		}
		return books;
	}

}