package theskillfactor.demo;

import static org.elasticsearch.node.NodeBuilder.*;

import java.util.UUID;

import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableAutoConfiguration
@ComponentScan
@EnableElasticsearchRepositories(basePackages = "theskillfactor.demo")
public class BooksMain {

	@Bean
	public ElasticsearchTemplate elasticsearchTemplate() {
		return new ElasticsearchTemplate(getNodeClient());
	}

	private static Client getNodeClient() {
		return nodeBuilder().settings(Settings.builder().put("path.home", "./es"))
				.clusterName(UUID.randomUUID().toString()).local(true).node().client();
	}

}