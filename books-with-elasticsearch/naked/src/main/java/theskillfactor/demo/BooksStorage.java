package theskillfactor.demo;

import java.util.List;

public interface BooksStorage {
	
	public interface Book {
		String title();
		String author();
	}
	
	void store(Book book);

	List<Book> search(String searchTerm);
}