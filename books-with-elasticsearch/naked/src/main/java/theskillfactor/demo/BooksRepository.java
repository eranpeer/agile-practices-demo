package theskillfactor.demo;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import theskillfactor.demo.BooksStorageImpl.BookDocument;

public interface BooksRepository extends ElasticsearchRepository<BookDocument,String> {
	List<BookDocument> findByAuthorOrTitle(String author, String title);
}