package theskillfactor.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.stereotype.Component;

@Component
class BooksStorageImpl implements BooksStorage {

	private BooksRepository repository;

	public BooksStorageImpl(BooksRepository repository) {
		this.repository = repository;
	}

	private static final class DocWrapper implements Book {
		private BookDocument bookDocument;

		public DocWrapper(BookDocument bookDocument) {
			this.bookDocument = bookDocument;
		}

		@Override
		public String title() {
			return bookDocument.getTitle();
		}

		@Override
		public String author() {
			return bookDocument.getAuthor();
		}
	}

	@Document(indexName = "resource", type = "books")
	public static class BookDocument {

		@Id
		private String id;

		private String author;
		private String title;

		public String getAuthor() {
			return author;
		}

		public void setAuthor(String author) {
			this.author = author;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}
	}

	@Override
	public void store(Book book) {
		BookDocument doc = new BookDocument();
		doc.setAuthor(book.author());
		doc.setTitle(book.title());
		repository.save(doc);
	}

	@Override
	public List<Book> search(String searchTerm) {
		List<BookDocument> docs = repository.findByAuthorOrTitle(searchTerm, searchTerm);
		List<Book> result = new ArrayList<>();
		for (BookDocument bookDocument : docs) {
			result.add(new DocWrapper(bookDocument));
		}
		return result;
	}

	public void clear() {
		repository.deleteAll();
	}

}