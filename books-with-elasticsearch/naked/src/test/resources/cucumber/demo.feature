Feature: Search
  I want to search for books.

  Scenario Outline: Search
    Given the following indexed books:
      | title | author |
      | A     | X      |
      | B     | Y      |
    When I search for <SearchTerm>
    Then I should see the following book with <title> or <author>

    Examples: 
      | SearchTerm | title | author |
      | A          | A     | X      |
      | B          | B     | Y      |
      | X          | A     | X      |
      | Y          | B     | Y      |
      | C          |       |        |
