package postfix;

public class PostfixCalculatorImpl implements PostfixCalculator {

    StackCalculator stackCalculator;

    public PostfixCalculatorImpl() {
        stackCalculator = new StackCalculator();
    }

    public double calc(String expression) {
        stackCalculator.clear();

        StringBuilder tokenBuff = new StringBuilder();
        for (int i = 0; i < expression.length(); i++) {
            char c = expression.charAt(i);
            if (Character.isDigit(c)) {
                tokenBuff.append(c);
                continue;
            }
            pushOperandIfExists(tokenBuff);
            if (c == ',')
                continue;

            stackCalculator.applyOperator(c);
        }

        pushOperandIfExists(tokenBuff);
        return stackCalculator.top();
    }

    private void pushOperandIfExists(StringBuilder tokenBuff) {
        if (tokenBuff.length() > 0) {
            pushOperand(tokenBuff);
        }
    }

    private void pushOperand(StringBuilder tokenBuff) {
        double operand = Double.parseDouble(tokenBuff.toString());
        tokenBuff.setLength(0);
        stackCalculator.push(operand);
    }

}
