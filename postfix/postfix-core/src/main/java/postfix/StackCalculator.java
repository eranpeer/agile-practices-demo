package postfix;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.function.BinaryOperator;

/**
 * Created by Eran on 2/5/2017.
 */

class StackCalculator {

    private static final BinaryOperator<Double> plus = (l, r) -> l + r;
    private static final BinaryOperator<Double> minus = (l, r) -> l - r;
    private static final BinaryOperator<Double> mult = (l, r) -> l * r;
    private static final BinaryOperator<Double> div = (l, r) -> l / r;

    private static final Map<Character,BinaryOperator<Double>> operators = new HashMap<>();

    static {
        operators.put('+', plus);
        operators.put('-', minus);
        operators.put('*', mult);
        operators.put('/', div);
    }

    private Stack<Double> operands = new Stack<>();


    public void clear() {
        operands.clear();
    }

    public double top() {
        return operands.peek();
    }

    public void applyOperator(char operatorSign) {
        Double right = operands.pop();
        Double left = operands.pop();
        operands.push(operators.get(operatorSign).apply(left, right));

    }

    public void push(double operand) {
        operands.push(operand);
    }
}