package postfix;

public interface PostfixCalculator {

	double calc(String expression);

}