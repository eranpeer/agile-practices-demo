Feature: postfix 

Scenario Outline: Two operands expression 
	When i eveluate the expression <Expression> 
	Then the result should be <Result> 
	
	Examples: 
		| Expression | Result      |
		| 1,2+       | 3.0         |           
		| 2,1-       | 1.0         |           
		| 2,3*       | 6.0         |           
		| 6,3/       | 2.0         |           
		