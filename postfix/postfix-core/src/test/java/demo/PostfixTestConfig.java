package demo;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import postfix.PostfixCalculator;
import postfix.PostfixCalculatorImpl;

@Configuration
@EnableAutoConfiguration
public class PostfixTestConfig {
	@Bean
	PostfixCalculator postfixCalculator(){
		return new PostfixCalculatorImpl();
	}
}
