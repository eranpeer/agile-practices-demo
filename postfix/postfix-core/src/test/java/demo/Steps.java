package demo;

import org.junit.Assert;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import postfix.PostfixCalculator;

public class Steps {

	PostfixCalculator postfixCalculator;
	
	public Steps(PostfixCalculator postfixCalculator) {
		this.postfixCalculator = postfixCalculator;
	}

	private double actualResult;

	@When("^i eveluate the expression (.*)$")
	public void i_eveluate_the_expression(String expression) throws Throwable {
		actualResult = postfixCalculator.calc(expression);
	    
	}

	@Then("^the result should be (.*)$")
	public void the_result_should_be(double expectedResult) throws Throwable {
		Assert.assertEquals(expectedResult, actualResult,0);
	}

	
}
