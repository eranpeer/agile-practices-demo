package postfix;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class PostfixController implements PostfixWeb {

	private PostfixCalculator postfixCalculator;

	PostfixController(PostfixCalculator postfixCalculator) {
		this.postfixCalculator = postfixCalculator;
	}

	public double calc(@RequestParam(name = "expression", required = true) String expression) {
		return postfixCalculator.calc(expression);
	}
}
