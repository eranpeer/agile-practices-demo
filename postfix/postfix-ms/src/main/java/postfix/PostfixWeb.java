package postfix;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/")
public interface PostfixWeb {

	@RequestMapping(path = "calc", method = RequestMethod.POST)
	double calc(@RequestParam("expression") String expression);

}