package postfix;


import org.springframework.boot.SpringApplication;

public class PostfixMicroserviceMain {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(PostfixConfig.class, args);
	}
}