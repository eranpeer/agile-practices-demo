package postfix;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
//@ComponentScan
public class PostfixConfig {

	@Bean
	PostfixController postfixController(PostfixCalculator postfixCalculator) {
		return new PostfixController(postfixCalculator);
	}

	@Bean
	public PostfixCalculator postfixCalculator() {
		return new PostfixCalculatorImpl();
	}

}