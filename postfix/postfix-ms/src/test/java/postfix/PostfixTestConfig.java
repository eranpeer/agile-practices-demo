package postfix;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
// @ComponentScan
@EnableFeignClients(clients = { PostfixWebClient.class })
public class PostfixTestConfig {
	{
		int a = 0;
		a++;
	}

	@Bean
	PostfixCalculator postfixCalculator(final PostfixWeb web) {
		return new PostfixCalculator() {

			public double calc(String expression) {
				return web.calc(expression);
			}
		};
	}
}