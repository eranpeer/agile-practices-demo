package postfix;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions( //
		features = "classpath:"
		, glue = { //
				"demo", //
		}
		)
public class CucumberTests {

}