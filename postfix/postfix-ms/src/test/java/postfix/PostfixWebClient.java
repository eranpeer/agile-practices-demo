package postfix;

import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name = "PostfixWebClient", url = "localhost:8080")
public interface PostfixWebClient extends PostfixWeb {

}
