/**
 * 
 */
package util;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

public class TimeUtils {

	public static Date parse(String timeStr) {
		try {
			return DateFormat.getTimeInstance(DateFormat.SHORT, Locale.FRANCE)
					.parse(timeStr);
		} catch (ParseException e) {
		}
		return null;
	}

	public static int fromMinutes(int minutes) {
		return minutes * 60 * 1000;
	}


	public static long fromHours(int hours) {
		return fromMinutes(hours * 60);
	}

	public static int hours(long millis) {
		return (int) (((millis / 1000) / 60) / 60);
	}

}