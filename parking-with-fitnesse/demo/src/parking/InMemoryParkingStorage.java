package parking;

import java.util.HashMap;
import java.util.Map;

public class InMemoryParkingStorage implements ParkingStorage {

	private Map<Long, ParkingStorage.ParkingRecord> records = new HashMap<>();
	private long cardId = 0;

	public void addParkingRecord(ParkingStorage.ParkingRecord pr) {
		pr.setCardId(++cardId);
		records.put(pr.getCardId(), pr);
	}

	public ParkingStorage.ParkingRecord getParkingRecord(long cardId) {
		return records.get(cardId);
	}

}