package parking;

import java.util.Date;

public class ParkingService {

	public static interface Timer {
		Date currTime();
	}

	private Timer timer;
	private PerHourPaymentCalculator perHourPaymentCalculator;
	private ParkingStorage parkingStorage;

	public ParkingService(Timer timer, ParkingStorage storage) {
		this.timer = timer;
		perHourPaymentCalculator = new PerHourPaymentCalculator();
		parkingStorage = storage;
	}

	public long enterParking() {
		ParkingStorage.ParkingRecord pr = new ParkingStorage.ParkingRecord();
		Date entryTime = timer.currTime();
		pr.setEntryTime(entryTime);

		parkingStorage.addParkingRecord(pr);

		return pr.getCardId();
	}

	public int payParking(long cardId) {
		ParkingStorage.ParkingRecord pr = parkingStorage.getParkingRecord(cardId);
		Date exitTime = timer.currTime();
		return perHourPaymentCalculator.calcAmountToPay(pr.getEntryTime(), exitTime);
	}

}