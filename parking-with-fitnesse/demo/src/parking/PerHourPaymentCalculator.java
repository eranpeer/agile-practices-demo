package parking;

import java.util.Date;

import util.TimeUtils;

public class PerHourPaymentCalculator {

	private static final int HOUR_RATE = 10;

	public int calcAmountToPay(Date start, Date end) {
		long timeInParking = end.getTime() - start.getTime();
		return HOUR_RATE * hoursToPay(timeInParking);
	}

	private int hoursToPay(long timeInParking) {
		int hoursToPay = TimeUtils.hours(timeInParking +TimeUtils.fromMinutes(50));
		return hoursToPay;
	}
	
}