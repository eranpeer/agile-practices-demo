package parking;

import java.util.Date;

public interface ParkingStorage {

	public static class ParkingRecord {
	
		private long cardId;
		private Date entryTime;
	
		public void setCardId(long cardId) {
			this.cardId = cardId;
		}
	
		public long getCardId() {
			return cardId;
		}
	
		public void setEntryTime(Date entryTime) {
			this.entryTime = entryTime;
		}
	
		public Date getEntryTime() {
			return entryTime;
		}
	}

	void addParkingRecord(ParkingRecord pr);

	ParkingRecord getParkingRecord(long cardId);

}