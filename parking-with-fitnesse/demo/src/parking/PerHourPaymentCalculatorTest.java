package parking;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

public class PerHourPaymentCalculatorTest {

	PerHourPaymentCalculator paymentCalculator = new PerHourPaymentCalculator();

	@Test
	public void payNothingOnFirst10Minutes() {
		Date start = new Date(2014,10,16,10,0,0);
		Date end = new Date(2014,10,16,10,9,59);
		int amount = paymentCalculator.calcAmountToPay(start,end);
		assertEquals(0,amount);
	}

	@Test
	public void pay10OnAfterFirst10Minutes() {
		Date start = new Date(2014,10,16,10,0,0);
		Date end = new Date(2014,10,16,10,10,0);
		int amount = paymentCalculator.calcAmountToPay(start,end);
		assertEquals(10,amount);
	}

	@Test
	public void pay20OnAfterFirst10MinutesOfSecondHour() {
		Date start = new Date(2014,10,16,10,0,0);
		Date end = new Date(2014,10,16,11,10,0);
		int amount = paymentCalculator.calcAmountToPay(start,end);
		assertEquals(20,amount);
	}
	
}
