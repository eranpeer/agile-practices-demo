package fixtures;

import java.util.Date;

import parking.InMemoryParkingStorage;
import parking.ParkingService;
import parking.ParkingService.Timer;

public class TestContext {

	static final class TimerMock implements Timer {
	
		private Date currTime;
	
		public TimerMock() {
		}
	
		public Date currTime() {
			return currTime;
		}
	
		public void setCurrTime(Date currTime) {
			this.currTime = currTime;
		}
	}

	static  {
		timer = new TimerMock();
		TestContext.system = new ParkingService(TestContext.timer, new InMemoryParkingStorage());
	}
	
	static public TestContext.TimerMock timer;
	static public ParkingService system;

}