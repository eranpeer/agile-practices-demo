package fixtures;

import java.text.SimpleDateFormat;
import java.util.Date;

import fit.ColumnFixture;

public class ExitParking extends ColumnFixture {

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

	public long cardId;
	public String systemTime;
	
	private int amountToPay;

	public int amountToPay() {
		return amountToPay;
	}

	@Override
	public void execute() throws Exception {
		Date time  = dateFormat.parse(systemTime);
		TestContext.timer.setCurrTime(time);	
		amountToPay = TestContext.system.payParking(cardId);
	}

}