package fixtures;

import java.text.SimpleDateFormat;
import java.util.Date;

import fit.ColumnFixture;

public class EnterParking extends ColumnFixture {

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

	public String systemTime;
	private long producedCardId;
	
	public EnterParking() {
	}

	public long cardId() {
		return producedCardId;
	}

	@Override
	public void execute() throws Exception {
		Date currTime = dateFormat.parse(systemTime);
		TestContext.timer.setCurrTime(currTime);
		producedCardId = TestContext.system.enterParking();
	}

}