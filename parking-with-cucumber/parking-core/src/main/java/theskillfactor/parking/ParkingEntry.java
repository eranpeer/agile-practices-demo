package theskillfactor.parking;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "parking_entry")
public class ParkingEntry {
	long id;
	Date time;

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public Date getTime() {
		return time;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setTime(Date time) {
		this.time = time;
	}
}