package theskillfactor.parking;

import java.util.Date;

public interface ParkingStorage {

	long addEntry(Date entryTime);

	Date getEntry(long cardId);

}