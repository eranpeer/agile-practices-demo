package theskillfactor.parking;

import static org.query.Operators.*;

import java.util.Date;

import org.query.QueryOperations;


public class ParkingStorageImpl implements ParkingStorage {

	private QueryOperations query;

	
	public ParkingStorageImpl(QueryOperations query) {
		this.query = query;
	}

	public long addEntry(Date entryTime) {
		ParkingEntry entry = new ParkingEntry();
		entry.setTime(entryTime);
		query.insert(entry);
		long id = entry.getId();
		return id;
	}

	public Date getEntry(long cardId) {
		ParkingEntry entry = query.query(ParkingEntry.class).
			where(e -> property(e.getId()).Eq(cardId)).findOne();
		Date time = entry.getTime();
		return time;
	}	
}