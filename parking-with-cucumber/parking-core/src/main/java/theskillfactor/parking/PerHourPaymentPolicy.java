package theskillfactor.parking;

import java.util.Date;

public class PerHourPaymentPolicy implements PaymentPolicy {

	public int calcAmountToPay(Date entryTime, Date paymentTime) {
		long timeInParking = paymentTime.getTime() - entryTime.getTime();
		long hoursToPay = hoursToPay(timeInParking);
		return (int) (10 * hoursToPay);
	}

	private long hoursToPay(long timeInParking) {
		return (timeInParking + minutes(50))/minutes(60);
	}

	private int minutes(int min) {
		return min * 60 * 1000;
	}

}