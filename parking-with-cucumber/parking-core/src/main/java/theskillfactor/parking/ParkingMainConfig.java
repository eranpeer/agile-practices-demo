package theskillfactor.parking;

import org.query.QueryOperations;
import org.query.SpringJdbcQueryEngine;
import org.query.SqlDialect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcOperations;

@Configuration
public class ParkingMainConfig {

	@Bean
	@Scope("singleton")
	QueryOperations queryOperations(JdbcOperations jdbcOperations, SqlDialect sqlDialect) {
		return new QueryOperations(new SpringJdbcQueryEngine(jdbcOperations, sqlDialect));
	}

	@Bean
	ParkingService parkingService(Timer timer, ParkingStorage storage) {
		return new ParkingServiceImpl(timer, storage);
	}

	@Bean
	ParkingStorage parkingStorage(QueryOperations query) {
		return new ParkingStorageImpl(query);
	}
}