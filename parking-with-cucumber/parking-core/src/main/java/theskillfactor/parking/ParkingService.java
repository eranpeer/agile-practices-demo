package theskillfactor.parking;

public interface ParkingService {

	int calcPayment(long cardId);

	long produceCard();

}