package theskillfactor.parking;

import java.util.Date;

public class ParkingServiceImpl implements ParkingService {

	private Timer timer;
	private ParkingStorage storage;
	private PaymentPolicy paymentPolicy = new PerHourPaymentPolicy();

	public ParkingServiceImpl(Timer timer, ParkingStorage storage) {
		this.timer = timer;
		this.storage = storage;
	}

	public int calcPayment(long cardId) {
		Date paymentTime =  new Date(timer.currTime());
		Date time = storage.getEntry(cardId);
		return paymentPolicy.calcAmountToPay(time, paymentTime);
	}

	public long produceCard() {
		Date entryTime = new Date(timer.currTime());
		return storage.addEntry(entryTime);
	}

}