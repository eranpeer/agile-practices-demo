package theskillfactor.parking;

import java.util.Date;

public interface PaymentPolicy {

	int calcAmountToPay(Date entryTime, Date paymentTime);

}