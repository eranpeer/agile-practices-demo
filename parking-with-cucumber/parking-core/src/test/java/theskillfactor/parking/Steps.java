package theskillfactor.parking;

import static org.hamcrest.CoreMatchers.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.junit.Assert;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class Steps {

	private ParkingService parkingService;
	private ParkingServiceTestApi testApi;

	public Steps(ParkingService parkingService, ParkingServiceTestApi testApi) {
		this.parkingService = parkingService;
		this.testApi = testApi;
	}

	private long cardId;
	private int actualAmountToPay;

	@Given("^I entered the parking at (.*)$")
	public void i_entered_the_parking_at(String entryTimeStr) throws Throwable {
		Date entryTime = parseTime(entryTimeStr);
		testApi.setSystemTime(entryTime.getTime());
		cardId = parkingService.produceCard();
	}

	@Given("^I pay at (.*)$")
	public void i_pay_at(String paymentTimeStr) throws Throwable {
		Date paymentTime = parseTime(paymentTimeStr);
		testApi.setSystemTime(paymentTime.getTime());
		actualAmountToPay = parkingService.calcPayment(cardId);
	}

	@Then("^I should be asked to pay (\\d+)$")
	public void i_should_be_asked_to_pay(int expectedAmpuntToPay) throws Throwable {
		Assert.assertThat(actualAmountToPay, is(expectedAmpuntToPay));
	}

	private static Date parseTime(String entryTimeStr) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		return dateFormat.parse(entryTimeStr);
	}

	@After
	public void afterScenario() {
		testApi.afterTest();
	}

	@Before
	public void beforeScenario() {
		testApi.beforeTest();
	}

	@PostConstruct
	protected void init() {
	}
}