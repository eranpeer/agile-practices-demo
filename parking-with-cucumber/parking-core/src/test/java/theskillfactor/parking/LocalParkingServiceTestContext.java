package theskillfactor.parking;

public class LocalParkingServiceTestContext implements ParkingServiceTestContext {

	private final CleanupStrategy cleanupStrategy;
	private long systemTime;

	public LocalParkingServiceTestContext(CleanupStrategy cleanupStrategy) {
		this.cleanupStrategy = cleanupStrategy;
	}

	public Timer makeTimer() {
		return new Timer() {
			@Override
			public long currTime() {
				return systemTime;
			}
		};
	}

	public ParkingServiceTestApi getTestApi() {
		return new ParkingServiceTestApi() {
			public void setSystemTime(long time) {
				systemTime = time;
			}

			@Override
			public void beforeTest() {
				cleanupStrategy.beforeTest();
			}

			@Override
			public void afterTest() {
				cleanupStrategy.afterTest();
			}
		};
	}
}