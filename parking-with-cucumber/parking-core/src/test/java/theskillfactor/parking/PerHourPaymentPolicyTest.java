package theskillfactor.parking;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

public class PerHourPaymentPolicyTest {

	PaymentPolicy paymentPolicy = new PerHourPaymentPolicy();

	@Test
	public void shouldPayNothingOnFirst10Min() {
		Date entry = new Date(2000, 0, 1, 10, 0, 0);
		Date paymentTime = new Date(2000, 0, 1, 10, 9, 59);
		Assert.assertEquals(0, paymentPolicy.calcAmountToPay(entry, paymentTime));
	}


	@Test
	public void shouldPay10AfterFirst10Min() {
		Date entry = new Date(2000, 0, 1, 10, 0, 0);
		Date paymentTime = new Date(2000, 0, 1, 10, 10, 0);
		Assert.assertEquals(10, paymentPolicy.calcAmountToPay(entry, paymentTime));
	}

	@Test
	public void shouldPay20AfterFirst10MinOfSecondHour() {
		Date entry = new Date(2000, 0, 1, 10, 0, 0);
		Date paymentTime = new Date(2000, 0, 1, 11, 10, 0);
		Assert.assertEquals(20, paymentPolicy.calcAmountToPay(entry, paymentTime));
	}

}