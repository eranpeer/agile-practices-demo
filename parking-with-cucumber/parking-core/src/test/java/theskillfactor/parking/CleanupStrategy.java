package theskillfactor.parking;

public interface CleanupStrategy {

	void afterTest();

	void beforeTest();

}