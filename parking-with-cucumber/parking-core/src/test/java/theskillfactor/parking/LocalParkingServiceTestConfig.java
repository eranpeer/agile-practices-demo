package theskillfactor.parking;

import org.query.HsqlDialect;
import org.query.SqlDialect;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableAutoConfiguration
@Import({ ParkingMainConfig.class })
public class LocalParkingServiceTestConfig {

	@Bean
	ParkingServiceTestContext parkingServiceTestContext(PlatformTransactionManager cleanupStrategy) {
		return new LocalParkingServiceTestContext(new RollbackCleanupStrategy(cleanupStrategy));
	}

	@Bean
	Timer timer(ParkingServiceTestContext testContext) {
		return testContext.makeTimer();
	}

	@Bean
	ParkingServiceTestApi parkingServiceTestApi(ParkingServiceTestContext testContext) {
		return testContext.getTestApi();
	}

	@Bean
	SqlDialect sqlDialect() {
		return new HsqlDialect();
	}
}