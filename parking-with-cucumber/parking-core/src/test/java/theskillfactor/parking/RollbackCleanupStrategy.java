package theskillfactor.parking;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public class RollbackCleanupStrategy implements CleanupStrategy {

	private PlatformTransactionManager xactionManager;
	private TransactionStatus transactionStatus;

	public RollbackCleanupStrategy(PlatformTransactionManager xactionManager) {
		this.xactionManager = xactionManager;
	}

	public void afterTest() {
		xactionManager.rollback(transactionStatus);
		transactionStatus = null;

	}

	public void beforeTest() {
		transactionStatus = xactionManager.getTransaction(new DefaultTransactionDefinition());
	}
}