package theskillfactor.parking;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions( //
		features = "classpath:features", //
		glue = { //
				"theskillfactor.parking", //
				// enable spring transactions in tests.
				// used with @txn in feature file.
				//"cucumber.api.spring" 
		})
public class CucumberTests {

}
