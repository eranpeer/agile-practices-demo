package theskillfactor.parking;

public interface ParkingServiceTestApi {

	void setSystemTime(long time);

	void beforeTest();

	void afterTest();

}