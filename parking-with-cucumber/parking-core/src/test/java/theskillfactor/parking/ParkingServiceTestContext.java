package theskillfactor.parking;

/**
 * A Test context is a single context that holds the test-api and 
 * the mocks that are used for the test.
 */
public interface ParkingServiceTestContext {

	Timer makeTimer();

	ParkingServiceTestApi getTestApi();

}