Feature: payment 
	I want to pay in order to get out.

Scenario Outline: Per hour payment 
	Given I entered the parking at <EntryTime> 
	And I pay at <PaymentTime> 
	Then I should be asked to pay <AmountToPay> 
	
	Examples: 
		| EntryTime | PaymentTime | AmountToPay |
		| 10:00:00  | 10:09:59    |           0 |
		| 10:00:00  | 10:10:00    |          10 |
		| 10:00:00  | 11:09:59    |          10 |
		| 10:00:00  | 11:10:00    |          20 |
