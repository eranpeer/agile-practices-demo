package theskillfactor.parking_ms;

import org.springframework.boot.SpringApplication;

public class ParkingMicroserviceMain {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(ParkingMicroserviceConfig.class, args);
	}
}