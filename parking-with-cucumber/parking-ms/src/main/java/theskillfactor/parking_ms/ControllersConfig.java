package theskillfactor.parking_ms;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ControllersConfig {

	@Bean
	ParkingServiceController parkingController() {
		return new ParkingServiceController();
	}

}