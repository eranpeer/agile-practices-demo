package theskillfactor.parking_ms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import theskillfactor.parking.ParkingService;

@RestController
public class ParkingServiceController implements ParkingServiceWeb {

	@Autowired
	ParkingService parkingService;
	
	public static class Greeting {
		public String name;
	}

	
	public long produceCard() {
		return parkingService.produceCard();
	}

	@Override
	public int calcPayment(long cardId) {
		return parkingService.calcPayment(cardId);
	}
}
