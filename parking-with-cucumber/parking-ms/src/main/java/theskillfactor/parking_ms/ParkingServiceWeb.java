package theskillfactor.parking_ms;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/")
public interface ParkingServiceWeb {

	@RequestMapping(value = "/produce_card")
	long produceCard();

	@RequestMapping(method = RequestMethod.POST, path = "/calc_payment")
	int calcPayment(@RequestParam(name = "cardId", required = true) long cardId);

}