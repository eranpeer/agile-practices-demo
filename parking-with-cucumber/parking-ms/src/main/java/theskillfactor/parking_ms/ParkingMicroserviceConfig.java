package theskillfactor.parking_ms;

import org.query.HsqlDialect;
import org.query.SqlDialect;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import theskillfactor.parking.ParkingMainConfig;
import theskillfactor.parking.Timer;

@Configuration
@EnableAutoConfiguration
@Import({ ControllersConfig.class, ParkingMainConfig.class })
public class ParkingMicroserviceConfig {

	@Bean
	Timer timer() {
		return new Timer() {
			@Override
			public long currTime() {
				return System.currentTimeMillis();
			}
		};
	}

	@Bean
	SqlDialect sqlDialect() {
		return new HsqlDialect();
	}
}