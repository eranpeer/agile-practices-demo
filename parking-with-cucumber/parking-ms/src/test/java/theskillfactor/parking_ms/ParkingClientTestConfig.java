package theskillfactor.parking_ms;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.netflix.appinfo.CloudInstanceConfig;

import theskillfactor.parking.ParkingService;
import theskillfactor.parking.ParkingServiceTestApi;

@Configuration
@EnableFeignClients(clients = { //
		ParkingServiceTestApiWebClient.class, //
		ParkingServiceWebClient.class //
})
@ComponentScan
@EnableAutoConfiguration
@EnableEurekaClient	
public class ParkingClientTestConfig {

	static class ParkingServiceTestApiProxy implements ParkingServiceTestApi {
		private ParkingServiceTestApiWeb target;

		public ParkingServiceTestApiProxy(ParkingServiceTestApiWeb target) {
			super();
			this.target = target;
		}

		public void setSystemTime(long time) {
			target.setSystemTime(time);
		}

		public void beforeTest() {
			target.beforeTest();
		}

		public void afterTest() {
			target.afterTest();
		}
	}

	static class ParkingServiceProxy implements ParkingService {
		private ParkingServiceWeb target;

		public ParkingServiceProxy(ParkingServiceWeb target) {
			super();
			this.target = target;
		}

		public int calcPayment(long cardId) {
			return target.calcPayment(cardId);
		}

		public long produceCard() {
			return target.produceCard();
		}
	}

	@Bean
	ParkingService parkingService(ParkingServiceWeb target) {
		return new ParkingServiceProxy(target);
	}

	@Bean
	ParkingServiceTestApi parkingServiceTestApi(ParkingServiceTestApiWeb target) {
		return new ParkingServiceTestApiProxy(target);
	}
}