package theskillfactor.parking_ms;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.HttpMessageConvertersAutoConfiguration;
import org.springframework.cloud.netflix.feign.FeignAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import theskillfactor.parking.ParkingService;
import theskillfactor.parking.ParkingServiceTestApi;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { //
		FeignAutoConfiguration.class, //
		HttpMessageConvertersAutoConfiguration.class, //
		ParkingClientTestConfig.class //
})
public class SomeTest {

	@Autowired
	ParkingService parkingService;

	@Autowired
	ParkingServiceTestApi testApi;

	@Test
	public void testName() throws Exception {
		System.out.println(parkingService.produceCard());
		testApi.setSystemTime(0L);
	}

}