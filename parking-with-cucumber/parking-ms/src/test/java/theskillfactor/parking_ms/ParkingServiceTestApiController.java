package theskillfactor.parking_ms;

import java.util.Date;

import org.springframework.web.bind.annotation.RestController;

import theskillfactor.parking.ParkingServiceTestApi;

@RestController
public class ParkingServiceTestApiController implements ParkingServiceTestApiWeb {
	
	private ParkingServiceTestApi parkingServiceTestApi;

	public ParkingServiceTestApiController(ParkingServiceTestApi parkingServiceTestApi) {
		this.parkingServiceTestApi = parkingServiceTestApi;
	}

	public void setSystemTime(long time){
		parkingServiceTestApi.setSystemTime(new Date(time).getTime());
	}

	@Override
	public void beforeTest() {
		parkingServiceTestApi.beforeTest();
	}

	@Override
	public void afterTest() {
		parkingServiceTestApi.afterTest();
	}
}