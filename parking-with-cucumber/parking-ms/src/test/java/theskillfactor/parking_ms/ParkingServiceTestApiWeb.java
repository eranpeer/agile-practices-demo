package theskillfactor.parking_ms;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/")
public interface ParkingServiceTestApiWeb {

	@RequestMapping(method = RequestMethod.POST, path = "/set_system_time")
	void setSystemTime(@RequestParam(name = "time", required = true) long time);

	@RequestMapping(method = RequestMethod.POST, path = "/start_xaction")
	void beforeTest();

	@RequestMapping(method = RequestMethod.POST, path = "/rollback_xaction")
	void afterTest();

}