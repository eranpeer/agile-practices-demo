package theskillfactor.parking_ms;

import org.query.HsqlDialect;
import org.query.QueryOperations;
import org.query.SqlDialect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import theskillfactor.parking.LocalParkingServiceTestContext;
import theskillfactor.parking.ParkingServiceTestApi;
import theskillfactor.parking.ParkingServiceTestContext;
import theskillfactor.parking.Timer;

@Configuration
@Import({ ParkingMicroserviceConfig.class })
public class ParkingMicroserviceTestConfig {

	@Bean
	ParkingServiceTestContext parkingServiceTestContext(QueryOperations query) {
		return new LocalParkingServiceTestContext(new DeleteDataCleanupStrategy(query));
	}

	@Bean
	Timer timer(ParkingServiceTestContext testContext) {
		return testContext.makeTimer();
	}

	@Bean
	ParkingServiceTestApi parkingServiceTestApi(ParkingServiceTestContext testContext) {
		return testContext.getTestApi();
	}

	@Bean
	SqlDialect sqlDialect() {
		return new HsqlDialect();
	}

	@Bean
	ParkingServiceTestApiController testApiController(ParkingServiceTestApi parkingServiceTestApi) {
		return new ParkingServiceTestApiController(parkingServiceTestApi);
	}

}