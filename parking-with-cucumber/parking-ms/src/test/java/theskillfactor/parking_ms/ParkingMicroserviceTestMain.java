package theskillfactor.parking_ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableAutoConfiguration
@Import({ ParkingMicroserviceTestConfig.class })
@EnableEurekaClient
public class ParkingMicroserviceTestMain {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ParkingMicroserviceTestMain.class, args);
	}
}