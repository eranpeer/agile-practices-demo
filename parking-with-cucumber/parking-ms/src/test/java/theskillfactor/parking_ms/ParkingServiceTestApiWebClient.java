package theskillfactor.parking_ms;

import org.springframework.cloud.netflix.feign.FeignClient;

//@FeignClient(name = "TimerTestApiRestClient", url = "localhost:8080")
@FeignClient("parking-ms")
public interface ParkingServiceTestApiWebClient extends ParkingServiceTestApiWeb {
}