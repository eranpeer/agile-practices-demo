package theskillfactor.parking_ms;

import org.query.QueryOperations;

import theskillfactor.parking.CleanupStrategy;
import theskillfactor.parking.ParkingEntry;

public class DeleteDataCleanupStrategy implements CleanupStrategy {

	private QueryOperations query;

	public DeleteDataCleanupStrategy(QueryOperations query) {
		this.query = query;
	}

	public void afterTest() {
		query.delete(ParkingEntry.class).execute();
	}

	public void beforeTest() {
	}
}