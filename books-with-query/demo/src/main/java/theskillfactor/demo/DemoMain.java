package theskillfactor.demo;

import org.query.HsqlDialect;
import org.query.QueryOperations;
import org.query.SpringJdbcQueryEngine;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@ComponentScan
@EnableTransactionManagement
public class DemoMain {

	@Bean

	@Scope("singleton")
	QueryOperations queryOperations(JdbcOperations jdbcOperations) {
		return new QueryOperations(new SpringJdbcQueryEngine(jdbcOperations, new HsqlDialect()));
	}

}