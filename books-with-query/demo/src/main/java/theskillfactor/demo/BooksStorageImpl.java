package theskillfactor.demo;

import static org.query.Operators.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Table;

import org.query.QueryOperations;
import org.springframework.stereotype.Component;

@Component
class BooksStorageImpl implements BooksStorage {

	QueryOperations query;

	public BooksStorageImpl(QueryOperations query) {
		this.query = query;
	}

	private static final class DocWrapper implements Book {
		private BookDocument bookDocument;

		public DocWrapper(BookDocument bookDocument) {
			this.bookDocument = bookDocument;
		}

		@Override
		public String title() {
			return bookDocument.getTitle();
		}

		@Override
		public String author() {
			return bookDocument.getAuthor();
		}
	}

	@Table(name = "book")
	public static class BookDocument {

		private String author;
		private String title;

		public String getAuthor() {
			return author;
		}

		public void setAuthor(String author) {
			this.author = author;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}
	}

	@Override
	public void store(Book book) {
		BookDocument doc = new BookDocument();
		doc.setAuthor(book.author());
		doc.setTitle(book.title());
		query.insert(doc);
	}

	@Override
	public List<Book> search(String searchTerm) {
		List<BookDocument> docs = query.query(BookDocument.class).where(b -> 
			or(property(b.getTitle()).Eq(searchTerm),property(b.getAuthor()).Eq(searchTerm)))
				.findAll();
		List<Book> result = new ArrayList<>();
		for (BookDocument bookDocument : docs) {
			result.add(new DocWrapper(bookDocument));
		}
		return result;
	}

	public void clear() {
		query.delete(BookDocument.class).execute();
	}

}