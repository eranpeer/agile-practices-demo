package theskillfactor.demo;

import java.util.function.Consumer;

public interface JsonSource {
	void setDataHandler(Consumer<String> handler);
}