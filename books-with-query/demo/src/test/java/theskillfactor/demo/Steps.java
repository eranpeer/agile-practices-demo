package theskillfactor.demo;

import java.util.List;

import javax.annotation.PostConstruct;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import theskillfactor.demo.BooksStorage.Book;

@ContextConfiguration(classes = { DemoMain.class, Steps.Config.class })
// @Transactional
public class Steps {

	public static class Config {

		@Bean
		JsonSource jsonSource() {
			return new JsonSourceFake();
		}
	}

	@After
	protected void cleanup() {
		booksStorage.clear();
	}

	@PostConstruct
	protected void init() {
	}

	@Autowired
	JsonSourceFake dataSource;

	@Autowired
	BooksIndexer booksIndexer;

	@Autowired
	BooksStorageImpl booksStorage;

	private List<Book> books;

	public static class BookLine {
		public String title;
		public String author;

		public String getTitle() {
			return title;
		}
		
		public void setTitle(String title) {
			this.title = title;
		}
		
		public String getAuthor() {
			return author;
		}
		
		public void setAuthor(String author) {
			this.author = author;
		}
	}

	@Given("^the following indexed books:$")
	public void the_following_indexed_books(List<BookLine> books) throws Throwable {
		String inputJson = toJson(books);
		dataSource.simulateDataOnSocket(inputJson);
	}

	private String toJson(List<BookLine> books) {
		JsonArray array = new JsonArray();
		for (BookLine bookLine : books) {
			JsonObject bookObj = toJsonObject(bookLine);
			array.add(bookObj);
		}
		return array.toString();
	}

	private JsonObject toJsonObject(BookLine bookLine) {
		JsonObject bookObj = new JsonObject();
		bookObj.addProperty("title", bookLine.getTitle());
		bookObj.addProperty("author", bookLine.getAuthor());
		return bookObj;
	}

	@When("^I search for (.*)$")
	public void i_search_for(String searchTerm) throws Throwable {
		books = booksStorage.search(searchTerm);
	}

	@Then("^I should see the following book with (.*) or (.*)$")
	public void i_should_see_the_following_book_with(String title, String author) throws Throwable {
		if (title.equals("") && author.equals("")) {
			Assert.assertTrue(books.isEmpty());
			return;
		}
		Book book = books.get(0);
		Assert.assertEquals(title, book.title());
		Assert.assertEquals(author, book.author());
	}
}