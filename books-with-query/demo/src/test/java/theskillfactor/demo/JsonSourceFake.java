package theskillfactor.demo;

import java.util.function.Consumer;

public class JsonSourceFake implements JsonSource {

	private Consumer<String> handler;

	@Override
	public void setDataHandler(Consumer<String> handler) {
		this.handler = handler;
	}
	
	void simulateDataOnSocket(String json){
		handler.accept(json);
	}
}